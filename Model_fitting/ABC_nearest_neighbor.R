##########################################
##  Find set of 5800 nearest neighbors  ##
## to the 58 real data points among the ##
##    100,000 prior simulated points    ##
##########################################
library(scales)
source("../Helper.R")
#data
load("sample.rds")
load("../Systematic_review_data/Review_data.rds")

#cleaning
data <- Review_data[, c("Pf_prev (%) all", "Pv_prev (%) all", "Mixed_prev (%)", "method2", "sample size")]
colnames(data) <- c("Pf", "Pv", "PfPv", "is_PCR", "sample_size")
data <- data[data$is_PCR == "PCR",]

#define prevalences
sample$Pf_dis <- sample$Pf - sample$PfPv # disjoint Pf prevalence
sample$Pv_dis <- sample$Pv - sample$PfPv # disjoint Pv prevalence
sample$Neg    <- 1 - sample$Pf_dis - sample$Pv_dis - sample$PfPv #infection-free prevalence

#find the nearest neighbors
NN <- data.frame()
for (j in 1:nrow(data)) { #loop through 58 real data points
  n = data$sample_size[j] # extract the sample size of the real data point
  points <- data.frame()
  for(i in 1:nrow(sample)){ #loop through the 100.000 deterministic simulated points
    draw <- rmultinom(1, n, c(sample$Pf_dis[i], sample$Pv_dis[i], sample$PfPv[i], sample$Neg[i])) 
    # draw from multinomial with real data prevalence and simulated disjoint prevalence trio
    point <- data.frame("Pf" = (draw[1]+draw[3])/n, "Pv" = (draw[2]+draw[3])/n, "PfPv" = draw[3]/n) 
    #transform disjoint prevalence to joint prevalence
    distance <- eu_distance(sample = point, data = data, 1, j)
    #compute distance between the real data point and the stochatic simulated point
    points <- rbind(points, cbind(point, "distance" = distance))
    #save all the 100.000 stochastic simulated points and their distance to the real data point
  }
  points <- cbind(sample[, 1:3], points) #save the appropriate prior parameters
  NN <- rbind(NN, points[order(points$distance), ][1:100, ]) #save the 100 nearest neighbors for this specific data point
}
save(NN, file = "5800_nearest_neighbors.rds")

