###############################
##     P. falciparum and     ##
##   P. vivax co-infection   ##
## with biting heterogeneity ##
###############################
#with relapses and hypnozoites
#SETUP
source("Helper.R")
library(deSolve) #ODE solver
library(statmod) #GHQ solver
################
## PARAMETERS ## ---------------------------------------------------------------------------------------------------------------
################
{
#   #human
  bF = 1/2 #transmission efficiency M->H for P. falciparum
  bV = 1/2 #transmission efficiency M->H for P. vivax
  rF = 1/200
  rV = 1/60 #recovery rate for P. vivax
  k  = 1/383 #hypnozoite clearance rate
  g  = 1/50 #relapse rate
  #mosquito
  cF = 1/4 #transmission efficiency H->M for P. falciparum
  cV = 1/4 #transmission efficiency H->M for P. vivax
  nF = 1/10 #1/latency period for P. falciparum
  nV = 1/10 #1/latency period for P. vivax
  b  = d = 1/10 #birth rate and death rate
  #population level
  a  = 1/3 #average biting rate
  t  = seq(0,5000) #simulated time frame
}

###############
## FUNCTIONS ## ----------------------------------------------------------------------------------------------------------------
###############
#only selecting one heterogeneity group
names.by.het <- function(names, n){
  suffix <- vector()
  for(name in names){
    suffix <- c(suffix, substr(name, nchar(name), nchar(name)+1))
  }
  return(names[which(suffix == n)])
}

#heterogeneity function
#calculate nodes and weights with Gauss-Hermite Quadrature
heterogenize <- function(h, wx, mu, sigma){
  if(wx == "x"){
    # find the abscissa zi for the GHQ on Z ~ N(mu, sigma)
    z <- vector(length = h)
    z[1:h] = gauss.quad.prob(h, dist = "normal", mu = mu, sigma = sigma)$nodes #zi
    #exponentiate the zi to get the xi on the X ~ logN(mu, sigma)
    output = exp(z) #xi
  }else{
    # find the weights wi for the GHQ on Z ~ N(mu, sigma)
    w <- vector(length = h)
    w[1:h] = gauss.quad.prob(h, dist = "normal", mu = mu, sigma = sigma)$weights
    #weights do not change under exponentiation/log-transformation
    output = w
  }
  return(output)
}

######################
## WRAPPER FUNCTION ## ---------------------------------------------------------------------------------------------------------
######################

INF_het <- function(h, sigma, m, dial){ #ODE solver dependent on number of heterogeneity categories, h, and on the variance of the heterogeneity factor, sigma
  mu  = - (sigma^2)/2 # calculate the mean mu of Z ~ N(mu, sigma) if the mean of X ~ logN(mu, sigma) is 1
  x   = heterogenize(h, "x", mu, sigma) #xi, heterogeneity factor in each category
  w   = heterogenize(h, "w", mu, sigma) #wi, relative size of each category
  par = c(bF, bV, rF, rV, m, k, g, dial, nF, nV, b, d, a, h, x, w)
  
  #initial population proportions in compartments, corresponding to wi
  init <- vector(length = 9 + 8*h)
  init[1:9] = 1/9 #mosquito compartments equally filled
  init[1] = 1 #mosquito compartments equally filled
   for (l in 1:h) { # each heterogeneity group i should sum up (across all compartments) to the proportion wi
     init[(0:7)*h + l + 9] = w[l]/8 #within a heterogeneity group (that sums up to wi) all compartments are equally filled
   }
  #init[1:3 + 9] = w
  #names
  names_m = c("Sm", "EmF", "EmV", "EmFV", "ImFEmV", "ImVEmF", "ImF", "ImV", "ImFV") #names of mosquito compartments
  names_h = as.vector(t(outer(c("Sh", "IhF", "IhV", "IhH", "IhFV", "IhFH", "IhVH", "IhFVH"), 1:h, FUN = "paste0"))) #names of human compartments, pasted with 1,2,..,h for the respective heterogeneity group
  names   =  c(names_m, names_h) #all column names of the solution
  INF <- function(t,Y, par){ #function that calculates the derivatives at each time step
    
    #COMPARTMENTS
    #mosquito compartments, each compartment is one entry in Y
    Sm     = Y[1]
    EmF    = Y[2]
    EmV    = Y[3]
    EmFV   = Y[4]
    ImFEmV = Y[5]
    ImVEmF = Y[6]
    ImF    = Y[7]
    ImV    = Y[8]
    ImFV   = Y[9]
    #human compartments, each compartment is a range of entries in Y,
    #corresponding to the number of heterogeneity categories
    Sh    = Y[10:(10+h-1)] #for h=3, 10, 11, 12
    IhF   = Y[(10+h):(10+2*h-1)] #for h=3, 13, 14, 15
    IhV   = Y[(10+2*h):(10+3*h-1)] #for h=3, 16, 17, 18
    IhH   = Y[(10+3*h):(10+4*h-1)] #for h=3, 19, 20, 21
    IhFV  = Y[(10+4*h):(10+5*h-1)] #for h=3, 22, 23, 24
    IhFH  = Y[(10+5*h):(10+6*h-1)] #for h=3, 25, 26, 27
    IhVH  = Y[(10+6*h):(10+7*h-1)] #for h=3, 28, 29, 30
    IhFVH = Y[(10+7*h):(10+8*h-1)] #for h=3, 31, 32, 33
    
    #FOI
    #force of infection on mosquitoes
    LmF  = dial*cF * a * sum(x[1:h] * w[1:h] * (IhF[1:h] + IhFH[1:h])) + dial*cF * (1-(2-dial)*cV) * a * sum(x[1:h] * w[1:h] * (IhFV[1:h] + IhFVH[1:h]))
    LmV  = (2-dial)*cV * a * sum(x[1:h] * w[1:h] * (IhV[1:h] + IhVH[1:h])) + (2-dial)*cV * (1-dial*cF) * a * sum(x[1:h] * w[1:h] * (IhFV[1:h] + IhFVH[1:h]))
    LmFV = (2-dial)*cV * dial*cF * a * sum(x[1:h] * w[1:h] * (IhFV[1:h] + IhFVH[1:h]))
    #force of infection on humans
    LhF <- LhV <- LhFV <- vector(length = h)
    LhF[1:h]  = x[1:h] * a * m * (bF * (ImF + ImFEmV) + bF * (1-bV) * ImFV)
    LhV[1:h]  = x[1:h] * a * m * (bV * (ImV + ImVEmF) + bV * (1-bF) * ImFV)
    LhFV[1:h] = x[1:h] * a * m * bF * bV * ImFV

    
    #System of ODE Mosquitoes
    dSm     = b - d * Sm - LmF * Sm - LmFV * Sm - LmV * Sm 
    dEmF    =   - d * EmF + LmF * Sm - nF * EmF - (LmV + LmFV) * EmF  
    dEmV    =   - d * EmV + LmV * Sm - nV * EmV - (LmF + LmFV) * EmV 
    dEmFV   =   - d * EmFV + LmFV * Sm + (LmV + LmFV) * EmF + (LmF + LmFV) * EmV - nF * EmFV - nV * EmFV
    dImFEmV =   - d * ImFEmV + nF * EmFV - nV * ImFEmV + (LmV + LmFV) * ImF
    dImVEmF =   - d * ImVEmF + nV * EmFV - nF * ImVEmF + (LmF + LmFV) * ImV 
    dImF    =   - d * ImF + nF * EmF - (LmV + LmFV) * ImF 
    dImV    =   - d * ImV + nV * EmV - (LmF + LmFV) * ImV
    dImFV   =   - d * ImFV + nV * ImFEmV + nF * ImVEmF 
    
    #System of ODE Humans
    dSh <- dIhF <- dIhV <- dIhH <- dIhFV <- dIhFH <- dIhVH <- dIhFVH <- vector(length = h)
    dSh[1:h]    = rV * IhV[1:h] + k * IhH[1:h] + rF * IhF[1:h] - LhV[1:h] * Sh[1:h] - LhF[1:h] * Sh[1:h] - LhFV[1:h] * Sh[1:h]
    dIhF[1:h]   = LhF[1:h] * Sh[1:h] + rV * IhFV[1:h] + k * IhFH[1:h] - LhV[1:h] * IhF[1:h] - LhFV[1:h] * IhF[1:h] - rF * IhF[1:h]
    dIhV[1:h]   = k * IhVH[1:h] + rF * IhFV[1:h] - rV * IhV[1:h] - LhV[1:h] * IhV[1:h] - LhF[1:h] * IhV[1:h] - LhFV[1:h] * IhV[1:h]
    dIhH[1:h]   = rV * IhVH[1:h] + rF * IhFH[1:h] - LhF[1:h] * IhH[1:h] - k * IhH[1:h] - LhV[1:h] * IhH[1:h] - LhFV[1:h] * IhH[1:h] - g * IhH[1:h]
    dIhFV[1:h]  = LhF[1:h] * IhV[1:h]+ k * IhFVH[1:h] - LhV[1:h] * IhFV[1:h] - LhFV[1:h] * IhFV[1:h] - rF * IhFV[1:h] - rV * IhFV[1:h]
    dIhFH[1:h]  = rV * IhFVH[1:h] + LhF[1:h] * IhH[1:h] - rF * IhFH[1:h] - k * IhFH[1:h] - LhV[1:h] * IhFH[1:h] - LhFV[1:h] * IhFH[1:h] - g * IhFH[1:h]
    dIhVH[1:h]  = LhV[1:h] * Sh[1:h] + LhV[1:h] * IhV[1:h] + rF * IhFVH[1:h] + LhV[1:h] * IhH[1:h] + g * IhH[1:h] - rV * IhVH[1:h] - LhF[1:h] * IhVH[1:h] - LhFV[1:h] * IhVH[1:h] - k * IhVH[1:h]
    dIhFVH[1:h] = LhFV[1:h] * IhFV[1:h] + LhFV[1:h] * IhV[1:h] + LhFV[1:h] * IhVH[1:h] + LhFV[1:h] * IhF[1:h] + LhFV[1:h] * IhFH[1:h] + LhFV[1:h] * IhH[1:h] + LhV[1:h] * IhFV[1:h] + LhF[1:h] * IhVH[1:h] + LhV[1:h] * IhF[1:h] + LhV[1:h] * IhFH[1:h] + LhFV[1:h] * Sh[1:h] + g * IhFH[1:h] - rV * IhFVH[1:h] - rF * IhFVH[1:h] - k * IhFVH[1:h]
    
    #OUTPUT
    #list of computed derivatives
    return(list(c(dSm, dEmF, dEmV, dEmFV, dImFEmV, dImVEmF, dImF, dImV, dImFV,
                  dSh, dIhF, dIhV, dIhH, dIhFV, dIhFH, dIhVH, dIhFVH)))
  }
  sol <- lsoda(init,t,INF,par)
  colnames(sol) = c('time', names)
  return(list("sol" = sol, "names_h" = names_h, "names_m" = names_m))
}
